#ifndef QUEUE_LIST_H
#define QUEUE_LIST_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define TYPE_T int
typedef struct TreeNode
{
	TYPE_T data;
	struct TreeNode* left;
	struct TreeNode* right;
}TreeNode;

#define TYPE TreeNode*


typedef struct Node
{
	TYPE data;
	struct Node* next;
}Node;

static Node* crete_node(TYPE data)
{
	Node* node = malloc(sizeof(Node));
	node->data = data;
	node->next = NULL;
	return node;
}

typedef struct Queue
{
	Node* head;
	Node* tail;
	size_t size;
}Queue;

// 创建
Queue* creat_queue(void);

// 队空
bool empty_queue(Queue* queue);

// 出队
bool pop_queue(Queue* queue);

// 销毁
void destory_queue(Queue* queue);

// 入队
void push_queue(Queue* queue,TYPE data);

// 队头
TYPE* head_queue(Queue* queue);

// 队尾
TYPE* tail_queue(Queue* queue);

#endif//QUEUE_LIST_H