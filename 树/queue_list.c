#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "queue_list.h"

// 创建
Queue* creat_queue(void)
{
	Queue* queue = malloc(sizeof(Queue));
	queue->head = NULL;
	queue->tail = NULL;
	queue->size = 0;
	return queue;
}

// 队空
bool empty_queue(Queue* queue)
{
	return 0 == queue->size;
}


// 出队
bool pop_queue(Queue* queue)
{
	if(empty_queue(queue)) return false;

	Node* node = queue->head;
	queue->head = node->next;
	
	if(1 == queue->size) queue->tail = NULL;

	free(node);
	queue->size--;
	return true;
}


// 销毁
void destory_queue(Queue* queue)
{
	while(pop_queue(queue));
	free(queue);
}

// 入队
void push_queue(Queue* queue,TYPE data)
{
	Node* node = crete_node(data);
	if(empty_queue(queue))
	{
		queue->head = node;
		queue->tail = node;
	}
	else
	{
		queue->tail->next = node;
		queue->tail = node;
	}
	queue->size++;
}

// 队头
TYPE* head_queue(Queue* queue)
{
	if(empty_queue(queue)) return NULL;
	return &queue->head->data;
}

// 队尾
TYPE* tail_queue(Queue* queue)
{
	if(empty_queue(queue)) return NULL;
	return &queue->tail->data;
}