#include<stdio.h>
#include<getch.h>
#include "tools.h"
void clear_stdin(void)
{
	stdin->_IO_read_ptr = stdin->_IO_read_end;
}
char get_cmd(char start,char end)
{
	clear_stdin();
	printf("请输入指令:");
	while(true)
	{
		char val = getchar();
		if(val >= start && val <= end)
		{
			return val;
		}
	}
}
char* get_passwd(char* passwd,bool is_show,size_t size)
{
	clear_stdin();
	if(NULL==passwd) return NULL;
	int count=0;
	do{
		char val=getch();
		if(127==val)
		{
			if(count>0)
			{
				if(is_show) printf("\b \b");
				count--;
			}
			continue;
		}
		else if(10==val)
		{
			break;
		}
		passwd[count++]=val;
		if(is_show) printf("*");
	}while(count<size-1);
	passwd[count]='\0';
	return passwd;
}
