#ifndef TOOLS_H
#define TOOLS_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define pf printf
#define sf scanf
#define PASSWORD	"--zzxx--"
char get_cmd(char start,char end);
char* get_passwd(char* passwd,bool is_show,size_t size);
void clear_stdin(void);
#endif//TOOLS_H

