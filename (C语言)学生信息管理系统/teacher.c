#include<stdio.h>
#include<string.h>
#include<getch.h>
#include<stdbool.h>
#include<stdlib.h>
#include "list.h"
#include "tools.h"
#include "teacher.h"
#include<unistd.h>
#include<signal.h>
typedef struct Student
{
	char name[20];
	int id;
	int age;
	char sex;
	float score;
}Student;
typedef struct Teacher
{
	char name[20];
	char id[20];
	char pw[20];
	int age;
	char sex;
	int count;
}Teacher;
Teacher* TEA;
Student* STU;
List* list_tea=NULL;
List* list_stu=NULL;
/*void show_tea(void)
{
	void show(const void* ptr)
	{
		const Teacher* TEA=ptr;
		printf("%s %s %s %d %c\n",TEA->name,TEA->id,TEA->pw,TEA->age,TEA->sex);
	}
	show_list(list_tea,show);
}*/
void show_stu(void)
{
	void show(const void* ptr)
	{
		const Student* stu=ptr;
		printf("%s %d %d %c %f\n",stu->name,stu->id,stu->age,stu->sex,stu->score);
	}
	show_list(list_stu,show);
}
void sigint(int signum)
{
	printf("你按了%u",signum);
}
void sigquit(int signum)
{

	printf("你按了%u",signum);
}
void tea_init(void)
{
	if(system("./jm.out --zzxx-- teacher"));
	signal(SIGINT,sigint);
	signal(SIGQUIT,sigquit);
	sigset_t set,oldset;
	sigemptyset(&set);
	sigaddset(&set,SIGINT);
	sigaddset(&set,SIGQUIT);
	sigprocmask(SIG_BLOCK,&set,&oldset);
	// 申请堆内存、加载数据
	FILE* frp=fopen("teacher","r");
	while(1)
	{
		TEA = malloc(sizeof(Teacher));
		if(fscanf(frp,"%s %s %s %d %c %d\n",TEA->name,TEA->id,TEA->pw,&TEA->age,&TEA->sex,&TEA->count)!=EOF)
		{
			add_tail_list(list_tea,TEA);
			sleep(1);
		}
		else
		{
			break;
		}
	}
	sigprocmask(SIG_SETMASK,&oldset,NULL);
}
void stu_init(void)
{
	// 申请堆内存、加载数据
	if(system("./jm.out --zzxx-- student"));
	signal(SIGINT,sigint);
	signal(SIGQUIT,sigquit);
    sigset_t set,oldset;
	sigemptyset(&set);
	sigaddset(&set,SIGINT);
	sigaddset(&set,SIGQUIT);
	sigprocmask(SIG_BLOCK,&set,&oldset);
	FILE* frp1=fopen("student","r");
	while(1)
	{
		STU = malloc(sizeof(Student));
		if(fscanf(frp1,"%s %d %d %c %f\n",STU->name,&STU->id,&STU->age,&STU->sex,&STU->score)!=EOF)
		{
			add_tail_list(list_stu,STU);
			sleep(1);
		}
		else
		{
			break;
		}
	}
	sigprocmask(SIG_SETMASK,&oldset,NULL);
}

void stu_save(void)
{
	Student* s;
	Node* node;
	FILE* frp2=fopen("student","w");
	for(node=list_stu->head;node!=NULL;node=node->next)
	{
		s=node->ptr;
		if(fprintf(frp2,"%s %d %d %c %f\n",s->name,s->id,s->age,s->sex,s->score));
	}
	fclose(frp2);
}
void tearch_menu(void)
{
	pf("进入用户子系统...\n");
	pf("1、添加学生\n");
	pf("2、删除学生\n");
	pf("3、查找学生\n");
	pf("4、修改学生信息\n");
	pf("5、对学生排序\n");
	pf("6、返回上一级\n");
	pf("-------------------\n");
}
int count=0;
Teacher* tea;
bool tea_login(void)
{
	Node* node;
	int flag=0;
	pf("请输入用户名\n");
	char id[20];
	char pw[20];
	if(sf("%s",id)>0)
	{
		pf("请输入密码：\n");
		get_passwd(pw,true,20);
			for(node=list_tea->head;node!=NULL;node=node->next)
			{
				flag=0;
				tea=node->ptr;
				if(strcmp(tea->id,id)==0)
				{
					if(strcmp(tea->pw,pw)==0)
					{
						count=0;		
						flag=1;	
						pf("\n登陆成功！\n");
						return true;
						break;
					}
				}
			}
			if(flag==0)
			{	
				count++;
				if(count<3)
				{
					pf("\n用户名或密码错误！你还有%d次机会！\n",3-count);
					tea_login();
				}
				else
				{
					pf("\n登陆失败！\n");
					count=0;
					return false;
				}				
			}
	}	
	return true;
}
void tea_add_stu(void)
{
	Student* stu=malloc(sizeof(Student));
	pf("请输入学生的姓名：\n");
	if(sf("%s",stu->name));
	clear_stdin();
	pf("请输入学生的学号：\n");
	if(sf("%d",&stu->id));
	clear_stdin();
	pf("请输入学生的年龄：\n");
	if(sf("%d",&stu->age));
	clear_stdin();
	pf("请输入学生的性别(f/m)：\n");
	if(sf("%c",&stu->sex));
	clear_stdin();
	pf("请输入学生的成绩：\n");
	if(sf("%f",&stu->score));
	clear_stdin();
	add_tail_list(list_stu,stu);
	pf("添加成功！\n");
}
void _quick_sort(void* arr[],size_t left,size_t right,compar cmp)
{
	if(left >= right) return;

	// 计算标杆的下标
	int pi = (left+right)/2;
	// 备份标杆的值
	void* pv = arr[pi];
	// 备份左右下标
	int l = left , r = right;

	// 左右下标相遇时结束
	while(l < r)
	{
		// 在标杆的左边寻找比它大的数据
		while(l<pi && 1!=cmp(arr[l],pv)) l++;
		if(l<pi) // 如果没有超出范围，说明找到比标杆大的值
		{
			// 与标杆交换位置，并记录新的标杆下标
			arr[pi] = arr[l];
			pi = l;
		}
		// 在标杆的右边寻找比它小的数据
		while(pi<r && 1!=cmp(pv,arr[r])) r--;
		if(pi<r) // 如果没有走出范围，说明找到比标杆小的值
		{
			arr[pi] = arr[r];
			pi = r;
		}
	}
	// 还原标杆的值
	arr[pi] = pv;
	//show_arr(arr,10);
	if(pi-left > 1) _quick_sort(arr,left,pi-1,cmp);
	if(right-pi > 1) _quick_sort(arr,pi+1,right,cmp);
}

void quick_sort(void* arr[],size_t len,compar cmp)
{
	_quick_sort(arr,0,len-1,cmp);
}

void tea_del_stu(void)
{
	clear_stdin();
	printf("请选择按姓名删除(n),还是按学号删除(i)：");
	if('n'==getch())
	{
		char name[20]={};
		printf("请输入要删除的姓名：\n");
		if(gets(name));
		int cmp(const void*  ptr1,const void* ptr2)
		{
			const Student* stu=ptr1;
			const char* name=ptr2;
			return strcmp(stu->name,name);
		}
		printf("删除%s\n",delete_value_list(list_stu,name,cmp)?"成功":"失败");
	}
	else if('i'==getch())
	{
		int id=0;
		printf("请输入要删除的学号：\n");
		if(scanf("%d",&id));
		int cmp(const void*  ptr1,const void* ptr2)
		{
			const Student* stu=ptr1;
			const int* id=ptr2;
			if(stu->id>*id)
				return 1;
			else if(stu->id<*id)
				return -1;
			else return 0;
		}
		printf("删除%s\n",delete_value_list(list_stu,&id,cmp)?"成功":"失败");
	}
	clear_stdin();
}
int _erfen(void* arr[],int start,int end,int key)
{
	if(start>end) return -1;
	int pi =(start+end)/2;
	Student* stu=arr[pi];
	if(key>stu->id) _erfen(arr,pi+1,end,key); 
	else if(key<stu->id) _erfen(arr,start,pi-1,key);
	else return pi;
	return -1;
}
int erfen(void* arr[],size_t len,int key)
{
	return _erfen(arr,0,len-1,key);
}
void tea_search_stu(void)
{
	clear_stdin();
	const Student* arr[list_stu->size];
	size_t index = 0;
	void get_stu(const void* ptr)
	{
		arr[index++] = ptr;
	}
	show_list(list_stu,get_stu);
	
	int cmp(const void* ptr1,const void* ptr2)
	{
		const Student* s1 = ptr1, * s2 = ptr2;
		if(s1->id > s2->id)
			return 1;
		else if(s1->id < s2->id)
			return -1;
		else
			return 0;
	}
	quick_sort((void**)arr,index,cmp);
	pf("请输入学生的学号\n");
	int key;
	if(sf("%d",&key));
	int i=erfen((void**)arr,list_stu->size-1,key);
	if(index>=0)
	{
		pf("%s %c %d\n",arr[i]->name,arr[i]->sex,arr[i]->id);
	}
	else
	{
		pf("查找失败\n");
	}
	
}
void tea_edit_stu(void)
{
	clear_stdin();
	int id=0;
	printf("请输入学号：\n");
	if(scanf("%d",&id));
	int cmp(const void*  ptr1,const void* ptr2)
	{
		const Student* stu=ptr1;
		const int* id=ptr2;
		if(stu->id>*id)
			return 1;
		else if(stu->id<*id)
			return -1;
		else return 0;
	}
	int i=find_list(list_stu,&id,cmp);
	if(i>=0)
	{
		Node* tmp=access_list(list_stu,i);
		Student* stu1=tmp->ptr;
		pf("%s %d %d %c %f\n",stu1->name,stu1->id,stu1->age,stu1->sex,stu1->score);
		pf("请输入修改后学生的成绩：");
		if(sf("%f",&stu1->score));
		pf("修改成功！\n");
	}
	else
		pf("该学生不存在\n");
	clear_stdin();
}

void tea_sort_stu(void)
{
	const Student* arr[list_stu->size];
	size_t index = 0;
	void get_stu(const void* ptr)
	{
		arr[index++] = ptr;
	}
	show_list(list_stu,get_stu);
	
	int cmp(const void* ptr1,const void* ptr2)
	{
		const Student* s1 = ptr1, * s2 = ptr2;
		if(s1->id > s2->id)
			return 1;
		else if(s1->id < s2->id)
			return -1;
		else
			return 0;
	}
	quick_sort((void**)arr,index,cmp);
	
	for(int i=0; i<index; i++)
	{
		pf("%s %c %d\n",arr[i]->name,arr[i]->sex,arr[i]->id);
	}
}
void tearch_start(void)
{
	list_tea=creat_list();
	list_stu=creat_list();
	tea_init();
	stu_init();
	if(!tea_login())
	{
		return;
	}
	while(1)
	{
		tearch_menu();
		switch(get_cmd('1','6'))
		{
			case '1': tea_add_stu();stu_save();break;
			case '2': tea_del_stu();stu_save();break;
			case '3': tea_search_stu(); break;
			case '4': tea_edit_stu();stu_save();break;
			case '5': tea_sort_stu();break;
			case '6': return;
		}
	}
}
