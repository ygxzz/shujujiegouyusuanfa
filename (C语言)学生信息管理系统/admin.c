#include<stdio.h>
#include<string.h>
#include<getch.h>
#include<stdbool.h>
#include<stdlib.h>
#include "list.h"
#include "tools.h"
#include "admin.h"
#include<unistd.h>
#include<signal.h>
typedef struct Teacher
{
	char name[20];
	char id[20];
	char pw[20];
	int age;
	char sex;
	int count;
}Teacher;
Teacher* TEA;
List* tea_list=NULL;
void show_tea(void)
{
	void show(const void* ptr)
	{
		const Teacher* TEA=ptr;
		printf("%s %s %s %d %c\n",TEA->name,TEA->id,TEA->pw,TEA->age,TEA->sex);
	}
	show_list(tea_list,show);
}
void sigint1(int signum)
{
	printf("你按了%u",signum);
}
void sigquit1(int signum)
{

	printf("你按了%u",signum);
}
void admin_init(void)
{
	// 申请堆内存、加载数据
	if(system("./jm.out --zzxx-- teacher"));
	signal(SIGINT,sigint1);
	signal(SIGQUIT,sigquit1);
	sigset_t set,oldset;
	sigemptyset(&set);
	sigaddset(&set,SIGINT);
	sigaddset(&set,SIGQUIT);
	sigprocmask(SIG_BLOCK,&set,&oldset);
	FILE* frp=fopen("teacher","r");
	while(1)
	{
		TEA = malloc(sizeof(Teacher));
		if(fscanf(frp,"%s %s %s %d %c %d\n",TEA->name,TEA->id,TEA->pw,&TEA->age,&TEA->sex,&TEA->count)!=EOF)
		{
			add_tail_list(tea_list,TEA);
			sleep(1);
		}	
		else
		{
			break;
		}
	}
	sigprocmask(SIG_SETMASK,&oldset,NULL);
}

void tea_save(void)
{
	Teacher* TEA;
	Node* node;
	FILE* frp2=fopen("teacher","w");
	for(node=tea_list->head;node!=NULL;node=node->next)
	{
		TEA=node->ptr;
		if(fprintf(frp2,"%s %s %s %d %c %d\n",TEA->name,TEA->id,TEA->pw,TEA->age,TEA->sex,TEA->count));
	}
	fclose(frp2);
}
void admin_menu(void)
{
	pf("进入超级用户子系统...\n");
	pf("1、添加用户\n");
	pf("2、删除用户\n");
	pf("3、修改用户\n");
	pf("4、返回上一级\n");
	pf("-------------------\n");
}
int count1=0;
Teacher* tea;
bool admin_login(void)
{
	pf("请输入用户名\n");
	char id[20];
	char pw[20];
	if(sf("%s",id)>0)
	{
		pf("请输入密码：\n");
		get_passwd(pw,true,20);
		if(strcmp(id,"admin")==0&&strcmp(pw,"admin")==0)
		{
			count1=0;
			pf("登录成功！\n");
			return true;
		}
		else
		{
			count1++;
			if(count1<3)
			{
				pf("\n用户名或密码错误！你还有%d次机会！\n",3-count1);
				admin_login();
			}
			else
			{
				pf("\n登陆失败！\n");
				count1=0;
				return false;
			}				
		}	
	}	
	return true;
}
void admin_add_tea(void)
{
	Teacher* tea=malloc(sizeof(Teacher));
	pf("请输入用户的姓名：\n");
	if(sf("%s",tea->name));
	clear_stdin();
	pf("请输入用户的年龄：\n");
	if(sf("%d",&tea->age));
	clear_stdin();
	pf("请输入用户的性别(f/m)：\n");
	if(sf("%c",&tea->sex));
	clear_stdin();
	strcpy(tea->id,"admin");
	strcpy(tea->pw,"admin");
	tea->count=0;
	add_tail_list(tea_list,tea);
	pf("添加成功！\n");
}
void admin_del_tea(void)
{
	clear_stdin();
	printf("请选择按姓名删除(n),还是按教工号删除(i)：");
	if('n'==getch())
	{
		char name[20]={};
		printf("请输入要删除的姓名：\n");
		if(gets(name));
		int cmp(const void*  ptr1,const void* ptr2)
		{
			const Teacher* tea=ptr1;
			const char* name=ptr2;
			return strcmp(tea->name,name);
		}
		printf("删除%s\n",delete_value_list(tea_list,name,cmp)?"成功":"失败");
	}
	else if('i'==getch())
	{
		char id[20]={};
		printf("请输入要删除的教工号：\n");
		if(scanf("%s",id));
		int cmp(const void*  ptr1,const void* ptr2)
		{
			const Teacher* tea=ptr1;
			const char* id=ptr2;
			return strcmp(tea->id,id);
		}
		printf("删除%s\n",delete_value_list(tea_list,id,cmp)?"成功":"失败");
	}
	clear_stdin();
}
void admin_edit_tea(void)
{
	clear_stdin();
	char id[20]={};
	printf("请输入教工号：\n");
	if(sf("%s",id));
	int cmp(const void*  ptr1,const void* ptr2)
	{
		const Teacher* tea=ptr1;
		const char* id=ptr2;
		return strcmp(tea->id,id);
	}
	int i=find_list(tea_list,id,cmp);
	if(i>=0)
	{
		Node* tmp=access_list(tea_list,i);
		Teacher* tea1=tmp->ptr;
		pf("%s %d %c\n",tea1->name,tea1->age,tea1->sex);
		pf("请输入修改后该用户的密码：");
		if(sf("%s",tea1->pw));
		pf("修改成功！\n");
	}
	else
		pf("该用户不存在\n");
	clear_stdin();
}
void admin_start(void)
{
	tea_list=creat_list();
	admin_init();
	show_tea();
	if(!admin_login())
	{
		return;
	}
	while(1)
	{
		admin_menu();
		switch(get_cmd('1','4'))
		{
			case '1': admin_add_tea();tea_save();break;
			case '2': admin_del_tea();tea_save();break;
			case '3': admin_edit_tea();tea_save();break;
			case '4': return;
		}
	}
}
